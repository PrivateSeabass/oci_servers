#!/bin/sh

podman run -p 8080:80 --rm -it -v nginx-files:/etc/nginx/ -v ./coreServers.json:/usr/share/nginx/html/coreServers.json:z,ro docker.io/library/nginx:alpine
